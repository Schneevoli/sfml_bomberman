#pragma once

#include <vector>
#include <SFML/Graphics.hpp>

#include "tile.h"
#include "destructableTile.h"
#include "walkableTile.h"
#include "noneWalkableTile.h"

using namespace std;

/*
This class manages drawing the level from a given tile map specifier
The tilemap specifier is a vector of ints where each ints specifies
a different tile in the finished tilemap
*/
class Level : public sf::Drawable, public sf::Transformable {
	public:
		Level(vector<int> _tileMapSpec) {
            this->tileMapSpec = _tileMapSpec;
            this->instantiateTileMap();
		}
        void instantiateTileMap(bool isResetting = false);

    private:
        vector<int> tileMapSpec;
        vector<Tile*> tileMap;
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
        {
            // apply the transform
            states.transform *= getTransform();

            // Draw all tiles for this tilemap
            for (int i = 0; i < tileMapSpec.size() - 1; i++) {
                target.draw(*tileMap[i], states);
            }
        }

};