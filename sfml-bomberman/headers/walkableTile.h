#pragma once

#include <SFML/Graphics.hpp>

#include "tile.h"
#include "resource.h"

/*
A simple tile where the players can walk upon which means
this tile only draws its corresponding sprite and has no collision whatsoever
*/
class WalkableTile : public Tile {
	public:
		WalkableTile(sf::Color _debugColor = sf::Color::Blue)
			: Tile(_debugColor) {
			// Load texture for tile from resource holder
			Resource* resource = Resource::getInstance();
			this->resetSprite(resource->walkableTexture);
			this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
		}
};