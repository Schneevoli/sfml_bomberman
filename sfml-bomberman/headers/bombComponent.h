#pragma once

#include <vector>

#include "gameObject.h"
#include "bomb.h"


/// <summary>
/// This component handles input management and spawning
/// for laying bombs in the level by the player
/// </summary>
class BombComponent {
	public:
		BombComponent(int _playerId = 1, float _MaxBombCooldown = 1.5f) {
			this->playerId = _playerId;
			this->MaxBombCooldown = _MaxBombCooldown;
			this->bombCooldown = 0;
		}
		void update(GameObject& owner);
		void spawnBomb(GameObject& owner);
		/// <summary>
		/// This keeps track of all current bombs that 
		/// are laying around. When a bomb is spawned
		/// it is added to this, when it explodes it is removed
		/// </summary>
		static std::vector<Bomb> currentBombs;


	private:
		/// <summary>
		/// The playerId of the player this component belongs to
		/// used in the collision check of the bombs created by this component
		/// </summary>
		int playerId;
		/// <summary>
		/// For tracking when the player is able to place a bomb again
		/// </summary>
		float bombCooldown;
		/// <summary>
		/// The actual cooldown time to wait before allowing to spawn a new bomb
		/// TODO: Think about making this static -> This could also be static on the player
		/// </summary>
		float MaxBombCooldown;
};