#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

/// <summary>
/// This is a singleton as we need to keep
/// all textures alive while the game is running
/// This would also work without a singleton and just
/// referencing the instance of the resource holder in all necessary
/// gameObjects, but this is one of the rare cases where using a singleton
/// makes the code cleaner.
/// </summary>
class Resource
{
public:
    static Resource* getInstance()
    {
        if (instance == NULL) {
            instance = new Resource();
        }
        // static Resource* instance = new Resource();
        return instance;
    }
    void initializeTextures();
    sf::Texture walkableTexture;
    sf::Texture noneWalkableTexture;
    sf::Texture destructableTexture;
    sf::Texture destructableTextureDamaged;
    sf::Texture player1Texture;
    sf::Texture player2Texture;
    // Textures for animations
    // Bombs
    std::vector<sf::Texture> bombAnim;
    sf::Texture bomb1;
    sf::Texture bomb2;
    sf::Texture bomb3;
    sf::Texture bomb4;
    // Explosions
    std::vector<sf::Texture> explosionAnim;
    sf::Texture explosion1;
    sf::Texture explosion2;
    sf::Texture explosion3;



private:
    static Resource* instance;
    Resource(const Resource&);
    Resource& operator=(const Resource&);
    Resource() {}
};