#pragma once

#include <iostream>
#include <vector>

#include "gameObject.h"

/**
Handles collision detection for gameObjects
where this component is implemented
*/
class CollisionComponent {
	public:
		/// <summary>
		/// Constructor for the collision component
		/// </summary>
		/// <param name="owner"> Entityt his component belongs to </param>
		CollisionComponent(GameObject* owner = NULL, bool _triggerOnly = false) {
			this->triggerOnly = _triggerOnly;
			if (owner == NULL) {
				return;
			}
			//if (owner == nullptr) return;
			// Every GameObject that implements this component
			// is put in a list so we can iterate over these
			// when checking collision (and not over ALL GameOjects)
			std::cout << "Adding gameObject to collider list: " << owner->name << std::endl;
			this->objectsWithCollider.push_back(owner);
			this->ownerIndex = this->objectsWithCollider.size() - 1;
		}
		/// <summary>
		/// This function iterates over all objects that have collision
		/// for the given owner object
		/// This means this function should NOT be called for every
		/// GameObject that implements collision but only for relevant
		/// GameObjects like for example the player objects
		/// </summary>
		/// <param name="owner"> The GameObject that calls this function (e.g. player)</param>
		bool update(GameObject& owner);
		/// <summary>
		/// The rectangle we get when the current GameObject collides with 
		/// another gameObjects that implements this component
		/// </summary>
		sf::FloatRect collisionBounds;
		/// <summary>
		/// This is a list that holds all gameobjects in the game
		/// that implement the collision component
		/// </summary>
		static std::vector<GameObject*> objectsWithCollider;
		/// <summary>
		/// The index for the position of the owner of 
		/// this component in the objectsWithCollider array
		/// </summary>
		int ownerIndex = 0;


	private:
		bool triggerOnly;

};