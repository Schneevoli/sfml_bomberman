#pragma once

#include "animationComponent.h"
#include "resource.h"
#include "collisionComponent.h"

/// <summary>
/// The explosion class. Explosions are spawned by bombs
/// after a certain amount of time.
/// </summary>
class Explosion : public GameObject {

	public:
		/// <summary>
		/// Different directions a bomb can travel to
		/// </summary>
		enum ExplosionDirection
		{
			TOP,
			BOTTOM,
			LEFT,
			RIGHT
		};
		Explosion(sf::Vector2f _spawnPosition, ExplosionDirection _direction = ExplosionDirection::TOP, int _playerId = 1)
		: GameObject(sf::Color::Cyan, "Explosion") {
			this->direction = _direction;
			this->playerId = _playerId;
			this->setPosition(_spawnPosition);
			// Load texture for the bomb for this player
			Resource* resource = Resource::getInstance();
			this->resetSprite(resource->explosion1);
			this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
			// Initialize components
			this->animationComponent = new AnimationComponent(resource->explosionAnim, static_cast<GameObject*>(this), 0.15f);
			this->collisionComponent = new CollisionComponent(NULL, true);
			// Initialize timers
			this->explosionTime = 0.5;
			this->explosionTimer = 0;
			this->spawnNextTime = 0.25;

			this->hasSpawnedNext = false;
		}
		void update(int element);
		int updateIndex = 0;
		static std::vector<Explosion> currentExplosions;
		static float ScaleFactor;


	private:
		AnimationComponent* animationComponent;
		CollisionComponent* collisionComponent;
		ExplosionDirection direction;
		int playerId;
		float explosionTimer;
		float explosionTime;
		float spawnNextTime;
		bool hasSpawnedNext;
		void spawnNextExplosion();
		void removeExplosion();
};