#pragma once

#include "gameObject.h"

/*
The base class for tiles
*/
class Tile : public GameObject {
    public:
        Tile(sf::Color _debugColor = sf::Color::Black, std::string _name = "Tile")
            : GameObject(_debugColor, _name) {
            using base = GameObject;
        }
        static float ScaleFactor;
};