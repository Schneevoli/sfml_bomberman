#pragma once

#include <SFML/Graphics.hpp>

#include "tile.h"
#include "healthComponent.h"
#include "collisionComponent.h"
#include "resource.h"

/**
* A tile that implements the health component 
* and can therefore be destroyed by bombs from players
*/
class DestructableTile : public Tile {
	public:
		DestructableTile(sf::Color _debugColor = sf::Color::Red, std::string _name = "DestructableTile")
			: Tile(_debugColor, _name) {
			// Load texture for this tile
			Resource* resource = Resource::getInstance();
			this->resetSprite(resource->destructableTexture);
			this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
			this->collisionComponent = new CollisionComponent(static_cast<GameObject*>(this));
			this->healthComponent = new HealthComponent(2, static_cast<GameObject*>(this));
		}
		HealthComponent* healthComponent;
		CollisionComponent* collisionComponent;
		// This should be handled by the health component
		// or perhaps a specialized child of the healthComponent?
		void takeDamage(int damage = 1);
};