#pragma once

#include <iostream>
#include <vector>

#include "gameObject.h"

/**
Handles durability for gameObjects
like destructable tiles or the player
*/
class HealthComponent {
public:
	/// <summary>
	/// Constructor for the health component
	/// </summary>
	/// <param name="_maxHealth"> The max health the entity should hve </param>
	/// <param name="_owner"> The entity this component belongs to</param>
	HealthComponent(int _maxHealth = 2, GameObject* _owner = NULL) {
		if (_owner != NULL) {
			this->owner = _owner;
		}
		this->maxHealth = _maxHealth;
		this->currentHealth = this->maxHealth;
	}
	void TakeDamage(int damage);
	void update();
	void reset();
	int currentHealth;

private:
	int maxHealth;
	GameObject* owner = NULL;
	/// <summary>
	/// This is to change the sprite of the gameObject
	/// after it got damaged (e.g. destructable tile)
	/// </summary>
	void ChangeDamageSprite();

};