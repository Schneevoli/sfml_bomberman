#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>

#include "player.h"
#include "gameState.h"

/*
This class handles input for all players
and passes the corresponding parameters 
to the player objects
*/
class Input {
    public: 
        void clearInput(Player& player, int playerId);
        void update(Player& player, int playerId);
        // We just use different functions for the different control schemas
        // this is the easiest approach at the moment as we dont need that much complexity.
        // Of course this would be smoother if it'd be more abstract.
        void updateSchema1(Player& player);
        void updateSchema2(Player& player);
        void clearSchema1(Player& player);
        void clearSchema2(Player& player);
        // This is for handling the input while in the "menus"
        void updateMenu(GameState& gameState);
};