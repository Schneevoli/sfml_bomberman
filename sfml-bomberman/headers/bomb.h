#pragma once

#include <vector>

#include "gameObject.h"
#include "resource.h"
#include "animationComponent.h"

/// <summary>
/// The actual bomb entity the playe can spawn
/// Has a cooldown until it explodes, then changes sprites 
/// and damages all objects with healthComponents it collides with
/// </summary>
class Bomb : public GameObject {
    public:
        Bomb(int _playerId = 1,
            float _bombCooldown = 1.5f,
            sf::Color _debugColor = sf::Color::Magenta,
            std::string _name = "Bomb")
            : GameObject(_debugColor, _name) {
            this->playerId = _playerId;
            this->bombCooldown = _bombCooldown;
            // Load texture for the bomb for this player
            Resource* resource = Resource::getInstance();
            this->resetSprite(resource->bomb1);
            this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
            // Initialize components
            this->animationComponent = new AnimationComponent(resource->bombAnim, static_cast<GameObject*>(this));
        }
        static float ScaleFactor;
        void update(int element);

    private:
        int playerId;
        float bombCooldown;
        AnimationComponent* animationComponent;
};