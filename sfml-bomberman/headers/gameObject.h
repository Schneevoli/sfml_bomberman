#pragma once

#include <string>
#include <iostream>

#include <SFML/Graphics.hpp>

/*
The base class for our component pattern
Only inherits the Drawable and Transformable class
so that every GameObject is something that can be 
transformed and rendered
*/
class GameObject : public sf::Drawable, public sf::Transformable
{
	public:
		GameObject(sf::Color _debugColor = sf::Color::Green, std::string _name = "GameObject") {
			this->debugColor = _debugColor;
			this->name = _name;
		}
		std::string name;
		sf::Sprite sprite;
		sf::Color debugColor;
		sf::Texture texture;
		sf::FloatRect getObjectBounds();
		void resetSprite(sf::Texture& texture);

	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			states.transform *= getTransform();

			// Draw Debug sphere
			sf::RectangleShape shape(sf::Vector2f(60.f, 60.f));

			// Set the shape color to the specified debug color
			shape.setFillColor(this->debugColor);
			// Only draw debug sphere when no texture for the sprite is set
			if (this->sprite.getTexture() == NULL) {
				target.draw(shape, states);
			}

			// Draw defined sprite
			target.draw(this->sprite, states);
		}
};