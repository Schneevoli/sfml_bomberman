#pragma once

#include <iostream>
#include <vector>

#include "resource.h"
#include "gameObject.h"
#include "collisionComponent.h"
#include "bombComponent.h"
#include "healthComponent.h"

/*
A player is a gameObject that can be manipulated through input
It also implements a collisionComponent for handling collision 
and a health Component for handling health
*/
class Player : public GameObject
{
    public:
        Player(sf::Color _debugColor = sf::Color::Green, 
            int _playerId = 1, 
            sf::Vector2f _startPosition = sf::Vector2f(60.f, 60.f),
            std::string _name = "Player")
        : GameObject(_debugColor, _name) {
            // Load texture for player
            Resource* resource = Resource::getInstance();
            switch (_playerId) {
                case 1:
                    this->resetSprite(resource->player1Texture);
                break;
                case 2:
                    this->resetSprite(resource->player2Texture);
                break;
            }
            this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
            // Move to start position
            this->move(_startPosition);
            // Assign playerId
            this->playerId = _playerId;
            // Set max velocity from static member
            this->maxVelocity = this->MaxVelocity;
            // Initialize components
            this->bombComponent = new BombComponent(_playerId);
            this->collisionComponent = new CollisionComponent();
            this->healthComponent = new HealthComponent(2, static_cast<GameObject*>(this));
            // Assign player as one of the current players
            Player::currentPlayers.push_back(*this);
        }
        int playerId = 0;
        float xDirection = 0;
        float yDirection = 0;
        float velocity = 1;
        bool isMoving = false;
        static float MaxVelocity;
        static float ScaleFactor;
        static std::vector<Player> currentPlayers;
        HealthComponent* healthComponent;
        void update();

    private:
        void movement();
        float speed = 3;
        float friction = 0.1f;
        float maxVelocity = 4;
        CollisionComponent* collisionComponent;
        BombComponent* bombComponent;
};