#pragma once

#include <vector>
#include <SFML/Graphics.hpp>

#include "gameObject.h"

/// <summary>
/// This component can be used
/// if the gameObject should be animated
/// instead of a static sprite
/// </summary>
class AnimationComponent {
	public:
		/// <summary>
		/// Constructor for the animationComponent
		/// </summary>
		/// <param name="_textures"> textures for the animation </param>
		/// <param name="_owner"> entity this component belongs to </param>
		AnimationComponent(std::vector<sf::Texture> &_textures, GameObject* _owner, float _oneFrameTime = 0.5f) {
			this->owner = _owner;
			this->textures = _textures;
			this->frames = (int)_textures.size();
			this->oneFrameTime = _oneFrameTime;
			this->owner->resetSprite(_textures[0]);
		}
		void update(GameObject& _owner, bool playOnce = false);

	private:
		GameObject* owner;
		std::vector<sf::Texture> textures;
		int animationStep = 0;
		int frames = 0;
		float frameTimer;
		float oneFrameTime;
		bool hasPlayed = false;
		//float currentFrameTime;
};