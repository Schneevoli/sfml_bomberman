#pragma once

#include <SFML/Graphics.hpp>

#include "tile.h"
#include "collisionComponent.h"

/*
This is the tile where the player cant walk onto
Meaning this implements a collisionComponent
*/
class NoneWalkableTile : public Tile {
	public:
		NoneWalkableTile(sf::Color _debugColor = sf::Color::White, std::string _name = "NoneWalkableTile")
		: Tile(_debugColor, _name) {
			// Load texture for this tile
			Resource* resource = Resource::getInstance();
			this->resetSprite(resource->noneWalkableTexture);
			this->sprite.setScale(sf::Vector2f(this->ScaleFactor, this->ScaleFactor));
			//this->collisionComponent = new CollisionComponent(dynamic_cast<GameObject*>(this));
			this->collisionComponent = new CollisionComponent(static_cast<GameObject*>(this));

		}
		CollisionComponent* collisionComponent;

	private:
};