#pragma once

#include "player.h"
#include "level.h"
#include "explosion.h"
#include "bomb.h"


/// <summary>
/// Holds the current information 
/// about the gameState
/// </summary>
class GameState {
	public:
		GameState() {
			if (!this->font.loadFromFile("assets/fonts/standard.ttf")) {
				std::cout << "Cannot load font file!" << std::endl;
			}
			if (!this->logoFont.loadFromFile("assets/fonts/logo.ttf")) {
				std::cout << "Cannot load font file!" << std::endl;
			}
			// Set values for the logo
			logo.setFont(this->logoFont);
			logo.setString("BOMBERMAN");
			logo.setCharacterSize(256);
			logo.setFillColor(sf::Color::Magenta);
			logo.setPosition(sf::Vector2f(200, 500));
			// Set values for the input action solicitation text
			this->setTextValues(this->inputActionSolicitation, 64, sf::Color::White);
			inputActionSolicitation.setString("Press 'Space' to continue");
			inputActionSolicitation.setPosition(sf::Vector2f(200, 800));
			// Set values for winner display text
			this->setTextValues(this->winnerDisplay, 96, sf::Color::White);
			winnerDisplay.setPosition(sf::Vector2f(200, 600));
			// Set values for health text for player 1
			this->setTextValues(this->player1Health, 48, sf::Color::Blue);
			player1Health.setOutlineColor(sf::Color::Black);
			player1Health.setOutlineThickness(2.5f);
			player1Health.setString("Player 1: <3 <3");
			player1Health.setPosition(sf::Vector2f(20, 20));
			// Set values for health text for player 2
			this->setTextValues(this->player2Health, 48, sf::Color::Yellow);
			player2Health.setOutlineThickness(2.5f);
			player2Health.setPosition(sf::Vector2f(1450, 20));
			player2Health.setString("Player 2: <3 <3");
		}
		/// <summary>
		/// Different game state types
		/// </summary>
		enum StateTypes
		{
			START,
			GAME,
			END
		};
		/// <summary>
		/// The actual current game state
		/// </summary>
		static StateTypes State;
		/// <summary>
		/// For transitioning to a new gameState
		/// </summary>
		static StateTypes NewState;
		/// <summary>
		/// The winner of the game
		/// </summary>
		static Player* Loser;
		/// <summary>
		/// This is for changing the gameState 
		/// This is used as we want to use a small 
		/// timer before transitioning the gameState
		/// </summary>
		static void ChangeGameState(StateTypes stateType);
		/// <summary>
		/// This is a utility boolean for transitioning between gameStates
		/// as we want a small delay between the transitions
		/// </summary>
		static bool IsTransitioning;

		void update(Player& player1, Player& player2);
		void reInitialize(Player& player1, Player& player2, Level& level);
		void setHasReinitialized();
		sf::Text logo;
		sf::Text inputActionSolicitation;
		sf::Text winnerDisplay;
		sf::Text player1Health;
		sf::Text player2Health;

	private:
		float stateTransitionTimer = 0.f;
		float stateTransitionTime = 1.f;
		bool hasReinitialized = false;
		sf::Font font;
		sf::Font logoFont;
		void setWinnerDisplayText();
		void setTextValues(sf::Text& text, int textSize, sf::Color textColor);

};

