#pragma once

#include "../../headers/destructableTile.h"
#include "../../headers/noneWalkableTile.h"

void DestructableTile::takeDamage(int damage)
{
	this->healthComponent->TakeDamage(damage);
	// If this destructable tile is damaged, show damaged sprite
	if (this->healthComponent->currentHealth == 1) {
		Resource* resource = Resource::getInstance();
		this->resetSprite(resource->destructableTextureDamaged);
		return;
	}
	// If this tile is destroyed, make it appear as a normal tile
	if (this->healthComponent->currentHealth == 0) {
		Resource* resource = Resource::getInstance();
		this->resetSprite(resource->walkableTexture);
		CollisionComponent::objectsWithCollider.erase(CollisionComponent::objectsWithCollider.begin() + this->collisionComponent->ownerIndex);
		for (int j = 0; j < CollisionComponent::objectsWithCollider.size(); j++) {
			if (CollisionComponent::objectsWithCollider[j]->name == "DestructableTile") {
				DestructableTile* tile = dynamic_cast<DestructableTile*>(CollisionComponent::objectsWithCollider[j]);
				tile->collisionComponent->ownerIndex = j;
			}
			if (CollisionComponent::objectsWithCollider[j]->name == "NoneWalkableTile") {
				NoneWalkableTile* tile = dynamic_cast<NoneWalkableTile*>(CollisionComponent::objectsWithCollider[j]);
				tile->collisionComponent->ownerIndex = j;
			}
		}
		return;
	}
}
