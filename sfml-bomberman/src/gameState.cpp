#pragma once

#include "../headers/gameState.h"

// Declare all static variables 
GameState::StateTypes GameState::State = GameState::StateTypes::START;
GameState::StateTypes GameState::NewState = GameState::StateTypes::START;
Player* GameState::Loser = NULL;
bool GameState::IsTransitioning = false;

void GameState::ChangeGameState(StateTypes stateType)
{
	GameState::IsTransitioning = true;
	GameState::NewState = stateType;
}

void GameState::update(Player& player1, Player& player2)
{
	if (player1.healthComponent->currentHealth == 1) {
		this->player1Health.setString("Player 1: <3");
	}
	if (player1.healthComponent->currentHealth == 0) {
		this->player1Health.setString("Player 1: >.<");
	}
	if (player2.healthComponent->currentHealth == 1) {
		this->player2Health.setString("Player 2: <3");
	}
	if (player2.healthComponent->currentHealth == 0) {
		this->player2Health.setString("Player 2: >.<");
	}
	//this->player1Health.setString("Player 1: " + std::to_string(player1.healthComponent->currentHealth));
	//this->player2Health.setString("Player 1: " + std::to_string(player2.healthComponent->currentHealth));
	
	if (GameState::IsTransitioning) {
		if (this->stateTransitionTimer < this->stateTransitionTime) {
			this->stateTransitionTimer = this->stateTransitionTimer + 0.016;
		}
		else {
			GameState::IsTransitioning = false;
			this->stateTransitionTimer = 0;
			GameState::State = GameState::NewState;
			if (GameState::State == StateTypes::END) {
				this->setWinnerDisplayText();
			}
		}
	}
}

void GameState::setWinnerDisplayText()
{
	std::string winner = "Player 1";
	if (GameState::Loser->name == "Player 1") {
		winner = "Player 2";
	}
	this->winnerDisplay.setString(winner + " is the winner!");
}

void GameState::setHasReinitialized()
{
	if (this->hasReinitialized) {
		this->hasReinitialized = false;
	}
}

void GameState::setTextValues(sf::Text& text, int textSize, sf::Color textColor)
{
	text.setFont(this->font);
	text.setCharacterSize(textSize);
	text.setFillColor(textColor);
}

void GameState::reInitialize(Player& player1, Player& player2, Level& level) {
	if (this->hasReinitialized == false) {
		BombComponent::currentBombs.clear();
		Explosion::currentExplosions.clear();
		player1.setPosition(sf::Vector2f(400.f, 800.f));
		player1.healthComponent->reset();
		player2.setPosition(sf::Vector2f(1200.f, 800.f));
		player2.healthComponent->reset();
		level.instantiateTileMap(true);
		this->player1Health.setString("Player 1: <3 <3");
		this->player2Health.setString("Player 2: <3 <3");
		this->hasReinitialized = true;
	}

}
