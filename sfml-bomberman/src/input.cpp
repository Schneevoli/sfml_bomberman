#pragma once

#include "../headers/input.h"

using namespace std;


void Input::clearInput(Player& player, int playerId)
{

}

void Input::update(Player& player, int playerId) {
    switch (playerId) {
        case 1:
            this->clearSchema1(player);
            this->updateSchema1(player);
        break;
        case 2:
            this->clearSchema2(player);
            this->updateSchema2(player);
        break;
        default:
            this->clearSchema1(player);
            this->updateSchema1(player);
        break;
    }
    this->clearInput(player, playerId);
}



void Input::updateSchema1(Player& player)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        player.isMoving = true;
        player.yDirection = -1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        player.isMoving = true;
        player.xDirection = -1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        player.isMoving = true;
        player.yDirection = 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        player.isMoving = true;
        player.xDirection = 1;
    }
}

void Input::updateSchema2(Player& player)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        player.isMoving = true;
        player.yDirection = -1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        player.isMoving = true;
        player.xDirection = -1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        player.isMoving = true;
        player.yDirection = 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        player.isMoving = true;
        player.xDirection = 1;
    }
}

void Input::clearSchema1(Player& player)
{
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        player.isMoving = false;
    }
}

void Input::clearSchema2(Player& player)
{
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        player.isMoving = false;
    }
    if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        player.isMoving = false;
    }
}

void Input::updateMenu(GameState& gameState)
{
    if (GameState::State == GameState::StateTypes::START) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            GameState::ChangeGameState(GameState::StateTypes::GAME);
        }
    }    
    if (GameState::State == GameState::StateTypes::END) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            GameState::ChangeGameState(GameState::StateTypes::START);
        }
    }
}



