#pragma once

#include "../headers/player.h"

// We initialize all static options first
float Player::MaxVelocity = 4;
float Player::ScaleFactor = 0.70f;
std::vector<Player> Player::currentPlayers;

void Player::update()
{
	this->movement();
	this->collisionComponent->update(*this);
	this->bombComponent->update(*this);
	this->healthComponent->update();
}

void Player::movement()
{
	if (this->velocity <= 0) {
		this->xDirection = 0;
		this->yDirection = 0;
	}

	if (this->velocity < this->maxVelocity && this->isMoving == true) {
		velocity += 0.25f;
	}

	if (this->velocity > 0) {
		this->velocity -= this->friction;
	}

	float x = this->xDirection * this->velocity;
	float y = this->yDirection * this->velocity;

	//this->move(x, y);
	sf::Vector2f newPosition = sf::Vector2f(
		this->getPosition().x + x,
		this->getPosition().y + y
	);
	this->setPosition(newPosition);
}
