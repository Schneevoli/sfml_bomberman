#pragma once

#include "../headers/bomb.h"
#include "../headers/player.h"
#include "../headers/explosion.h"


float Bomb::ScaleFactor = 3.75f;

void Bomb::update(int element)
{
	// Update the animationComponent
	this->animationComponent->update(*this);

	this->bombCooldown = this->bombCooldown - 0.016f;

	if (this->bombCooldown <= 0) {
		// We now spawn explosions which travel in all four directions
		/*
		Explosion* topExplosion = new Explosion(this->getPosition(), Explosion::ExplosionDirection::TOP, this->playerId);
		Explosion::currentExplosions.push_back(*topExplosion);
		topExplosion->updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion* bottomExplosion = new Explosion(this->getPosition(), Explosion::ExplosionDirection::BOTTOM, this->playerId);
		Explosion::currentExplosions.push_back(*bottomExplosion);
		bottomExplosion->updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion* leftExplosion = new Explosion(this->getPosition(), Explosion::ExplosionDirection::LEFT, this->playerId);
		Explosion::currentExplosions.push_back(*leftExplosion);
		leftExplosion->updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion* rightExplosion = new Explosion(this->getPosition(), Explosion::ExplosionDirection::RIGHT, this->playerId);
		Explosion::currentExplosions.push_back(*rightExplosion);
		rightExplosion->updateIndex = Explosion::currentExplosions.size() - 1;*/


		Explosion topExplosion(this->getPosition(), Explosion::ExplosionDirection::TOP, this->playerId);
		Explosion::currentExplosions.push_back(topExplosion);
		topExplosion.updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion bottomExplosion(this->getPosition(), Explosion::ExplosionDirection::BOTTOM, this->playerId);
		Explosion::currentExplosions.push_back(bottomExplosion);
		bottomExplosion.updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion leftExplosion(this->getPosition(), Explosion::ExplosionDirection::LEFT, this->playerId);
		Explosion::currentExplosions.push_back(leftExplosion);
		leftExplosion.updateIndex = Explosion::currentExplosions.size() - 1;
		Explosion rightExplosion(this->getPosition(), Explosion::ExplosionDirection::RIGHT, this->playerId);
		Explosion::currentExplosions.push_back(rightExplosion);
		rightExplosion.updateIndex = Explosion::currentExplosions.size() - 1;


		BombComponent::currentBombs.erase(BombComponent::currentBombs.begin() + element);

	}
}
