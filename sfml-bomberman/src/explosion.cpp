#pragma once

#include "../headers/explosion.h"
#include "../headers/player.h"

std::vector<Explosion> Explosion::currentExplosions;
float Explosion::ScaleFactor = 4.75f;

void Explosion::update(int element)
{
	if (this->explosionTimer > this->explosionTime) {
		this->removeExplosion();
		return;
	}

	this->animationComponent->update(*this, true);

	this->explosionTimer = this->explosionTimer + 0.016;
	std::cout << "Explosion " << element << " --- " << this->explosionTimer << std::endl;


	sf::FloatRect explosionBounds = this->getObjectBounds();
	for (int i = 0; i < Player::currentPlayers.size(); i++) {
		if (Player::currentPlayers[i].playerId != this->playerId) {
			sf::FloatRect playerBounds = Player::currentPlayers[i].getObjectBounds();
			if (explosionBounds.intersects(playerBounds)) {
				std::cout << "Bomb of player " << this->playerId <<
					" collided with player " << Player::currentPlayers[i].playerId << std::endl;
				// Player takes 1 damage if hit by bomb
				Player::currentPlayers[i].healthComponent->TakeDamage(1);
				Explosion::currentExplosions.erase(Explosion::currentExplosions.begin() + element);
				for (int j = 0; j < Explosion::currentExplosions.size(); j++) {
					Explosion::currentExplosions[j].updateIndex = j;
				}
				return;
			}
		}
	}

	bool hasCollided = this->collisionComponent->update(*this);
	if (hasCollided) {
		return;
	}


	if (this->explosionTimer > this->spawnNextTime && this->hasSpawnedNext == false) {
		this->spawnNextExplosion();
		this->hasSpawnedNext = true;
	}

	/*
	switch (this->direction) {
		case ExplosionDirection::TOP:
			this->move(0, -5);
			break;
		case ExplosionDirection::BOTTOM:
			this->move(0, 5);
			break;
		case ExplosionDirection::LEFT:
			this->move(-5, 0);
			break;
		case ExplosionDirection::RIGHT:
			this->move(5, 0);
			break;
		default:
			this->move(0, -5);
			break;
	}*/
}

void Explosion::removeExplosion()
{
	//Explosion::currentExplosions.erase(std::next(Explosion::currentExplosions.begin() + this->updateIndex));
	Explosion::currentExplosions.erase(Explosion::currentExplosions.begin() + this->updateIndex);
	for (int i = 0; i < Explosion::currentExplosions.size(); i++) {
		Explosion::currentExplosions[i].updateIndex = i;
	}
}

void Explosion::spawnNextExplosion()
{
	sf::Vector2f spawnPosition = this->getPosition();
	switch (this->direction) {
		case Explosion::ExplosionDirection::BOTTOM:
			spawnPosition.y = spawnPosition.y + 60.f;
			break;
		case Explosion::ExplosionDirection::TOP:
			spawnPosition.y = spawnPosition.y - 60.f;
			break;
		case Explosion::ExplosionDirection::LEFT:
			spawnPosition.x = spawnPosition.x - 60.f;
			break;
		case Explosion::ExplosionDirection::RIGHT:
			spawnPosition.x = spawnPosition.x + 60.f;
			break;
		default:
			spawnPosition.y = spawnPosition.y - 60.f;
			break;
	}
	Explosion nextExplosion = Explosion(spawnPosition, this->direction, this->playerId);
	Explosion::currentExplosions.push_back(nextExplosion);
	nextExplosion.updateIndex = Explosion::currentExplosions.size() - 1;
}
