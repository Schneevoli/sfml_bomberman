#pragma once

#include "../headers/gameObject.h"

sf::FloatRect GameObject::getObjectBounds()
{
	// When no sprite is defined for the gameObject return early
	if (this->sprite.getTexture() == NULL) {
		return sf::FloatRect();
	}
	// Get the "real" bounds of this object
	// If we are just using sprite.getGlobalBounds() it wouldn't account
	// for the transformation we apply to the gameObject (and therefore always return 0)
	// As we are doing all our transformations on the gameObject we need to account for its transformation
	// in relation to the sprite
	sf::FloatRect objectBounds = this->getTransform().transformRect(this->sprite.getGlobalBounds());
	return objectBounds;
}

void GameObject::resetSprite(sf::Texture& texture)
{
	this->sprite.setTexture(texture);
}
