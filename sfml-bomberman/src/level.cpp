#pragma once

#include <SFML/Graphics.hpp>

#include "../headers/level.h"

void Level::instantiateTileMap(bool isResetting)
{
    // If the level should be reset, clear the array first
    if (isResetting) {
        this->tileMap.clear();
    }
    float xDirection = 0;
    float yDirection = 0;
    Resource* resource = Resource::getInstance();
    float pureTileSize = resource->destructableTexture.getSize().x;
    for (int i = 0; i < tileMapSpec.size(); i++) {
        switch (tileMapSpec[i]) {
            case 0: {
                WalkableTile* newTile = new WalkableTile();
                newTile->setPosition(xDirection, yDirection);
                tileMap.push_back(newTile);
                break;
            }
            case 1: {
                NoneWalkableTile* newTile = new NoneWalkableTile();
                newTile->setPosition(xDirection, yDirection);
                tileMap.push_back(newTile);
                // Set reference for collisions
                // TODO: Do this in the constructor if possible
                //newTile.collisionComponent.setOwner(newTile);
                break;
            }
            case 2: {
                DestructableTile* newTile = new DestructableTile();
                newTile->setPosition(xDirection, yDirection);
                tileMap.push_back(newTile);
                //newTile.collisionComponent.setOwner(newTile);
                break;
            }
        }
        xDirection += pureTileSize * Tile::ScaleFactor;
        // Step into next row if modulo of i+1 / 15 is 0 
        // as that means we have 15 tiles in the current row
        int checkForRows = i + 1;
        if (checkForRows % 15 == 0) {
            yDirection += pureTileSize * Tile::ScaleFactor;
            xDirection = 0;
        }
    }
}
