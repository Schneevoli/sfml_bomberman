#pragma once

#include "../../headers/animationComponent.h"

void AnimationComponent::update(GameObject& _owner, bool playOnce)
{
	// If the animation step has reached the amount of frames
	// reset the frameTimer and animationStep so the animation can loop again
	if (this->animationStep == this->frames) {
		hasPlayed = true;
		this->frameTimer = 0.f;
		this->animationStep = 0;
	}

	// If specified to only go through the animation once
	// return early if animation has played once
	if (playOnce == true && this->hasPlayed == true) {
		return;
	}

	this->frameTimer = this->frameTimer + 0.016f;

	float currentFrameTime = this->oneFrameTime * this->animationStep;
	if (this->frameTimer > currentFrameTime) {
		_owner.resetSprite(this->textures[this->animationStep]);
		this->animationStep = this->animationStep + 1;
	}
	
}
