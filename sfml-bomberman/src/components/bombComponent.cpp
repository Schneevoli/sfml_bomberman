#pragma once 

#include "../../headers/bombComponent.h"


std::vector<Bomb> BombComponent::currentBombs;


/// <summary>
/// TODO: Refactor this into the input module and just handle bomb spawning in here
/// </summary>
void BombComponent::update(GameObject& owner)
{
    if (this->bombCooldown > 0) {
        // 0.016 ~= 1 / 60 - should be refactored to use deltaTime
        this->bombCooldown = this->bombCooldown - 0.016f;
    }

    if (this->playerId == 1) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) &&
            this->bombCooldown <= 0)
        {
            spawnBomb(owner);
        }
    }

    if (this->playerId == 2) {
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt) && 
            this->bombCooldown <= 0) 
        {
            spawnBomb(owner);
        }
    }
}

/// <summary>
/// Spawns a bomb with the playerId of this component
/// </summary>
void BombComponent::spawnBomb(GameObject& owner)
{
    sf::Vector2f spawnPosition = owner.getPosition();
    Bomb newBomb(this->playerId);
    newBomb.setPosition(spawnPosition);
    BombComponent::currentBombs.push_back(newBomb);
    this->bombCooldown = this->MaxBombCooldown;
    std::cout << "Spawned bomb for player: " << owner.name << std::endl;
}
