#pragma once

#include "../../headers/collisionComponent.h"
#include "../../headers/player.h"
#include "../../headers/destructableTile.h"
#include "../../headers/explosion.h"

// Declare static variables first
std::vector<GameObject*> CollisionComponent::objectsWithCollider;


bool CollisionComponent::update(GameObject& owner)
{
	if (this->triggerOnly == false) {
		sf::FloatRect playerBounds = owner.getObjectBounds();
		// std::cout << playerBound.top << std::endl;
		for (int i = 0; i < CollisionComponent::objectsWithCollider.size(); i++) {
			// Get the bounds for the current iteration
			sf::FloatRect objectBounds = CollisionComponent::objectsWithCollider[i]->getObjectBounds();
			if (playerBounds.intersects(objectBounds)) {
				sf::Vector2f newPosition = owner.getPosition();
				if (playerBounds.top > objectBounds.top) {
					newPosition.y = newPosition.y + (Player::MaxVelocity / 1.5f);
				}
				if (playerBounds.top < objectBounds.top) {
					newPosition.y = newPosition.y - (Player::MaxVelocity / 1.5f);
				}
				if (playerBounds.left > objectBounds.left) {
					newPosition.x = newPosition.x + (Player::MaxVelocity / 1.5f);
				}
				if (playerBounds.left < objectBounds.left) {
					newPosition.x = newPosition.x - (Player::MaxVelocity / 1.5f);
				}
				owner.setPosition(newPosition);
				std::cout << "Collision detect for " << owner.name << " with " << CollisionComponent::objectsWithCollider[i]->name << std::endl;
				return true;
			}
		}
	}

	if (this->triggerOnly == true) {
		sf::FloatRect triggerBounds = owner.getObjectBounds();
		for (int i = 0; i < CollisionComponent::objectsWithCollider.size(); i++) {
			// Get the bounds for the current iteration
			sf::FloatRect objectBounds = CollisionComponent::objectsWithCollider[i]->getObjectBounds();
			if (triggerBounds.intersects(objectBounds)) {
				std::cout << "Collision detect for " << owner.name << " with " << CollisionComponent::objectsWithCollider[i]->name << std::endl;
				// If collided with a destructable tile either show damaged texture or "remove" it
				if (CollisionComponent::objectsWithCollider[i]->name == "DestructableTile") {
					DestructableTile* tile = dynamic_cast<DestructableTile*>(CollisionComponent::objectsWithCollider[i]);
					tile->takeDamage();
				}
				// If the owner was an explosion remove it from array
				if (owner.name == "Explosion") {
					Explosion* explosion = dynamic_cast<Explosion*>(&owner);
					Explosion::currentExplosions.erase(Explosion::currentExplosions.begin() + explosion->updateIndex);
					for (int j = 0; j < Explosion::currentExplosions.size(); j++) {
						Explosion::currentExplosions[j].updateIndex = j;
					}
				}
				return true;
			}
		}
	}
	return false;
}
