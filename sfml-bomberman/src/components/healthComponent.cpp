#pragma once

#include "../../headers/healthComponent.h"
#include "../../headers/resource.h"
#include "../../headers/gameState.h"

void HealthComponent::TakeDamage(int damage)
{
	this->currentHealth = this->currentHealth - damage;
	if (this->currentHealth <= 0) {
		std::cout << "Destroy this GameObject" << std::endl;
	}
}

void HealthComponent::update()
{
	if (this->currentHealth <= 0) {
		GameState::ChangeGameState(GameState::StateTypes::END);
		GameState::Loser = static_cast<Player*>(this->owner);
	}
}

void HealthComponent::reset()
{
	this->currentHealth = this->maxHealth;
}

void HealthComponent::ChangeDamageSprite()
{
	// TODO: This should be way more flexible
	std::string destructableTileName = "DestructableTile";
	if (this->owner->name == destructableTileName) {
		if (this->currentHealth < maxHealth) {
			Resource* resource = Resource::getInstance();
			this->owner->resetSprite(resource->destructableTexture);
		}
	}
}
