#pragma once

#include "../headers/resource.h"

Resource* Resource::instance = NULL;

void Resource::initializeTextures()
{
	if (!this->walkableTexture.loadFromFile("assets/tiles/walkableTileBig.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->noneWalkableTexture.loadFromFile("assets/tiles/nonWalkableTileBig.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->destructableTexture.loadFromFile("assets/tiles/destructableTileBig.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->destructableTextureDamaged.loadFromFile("assets/tiles/destructableTileBig_damaged.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->player1Texture.loadFromFile("assets/player/player1Big.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->player2Texture.loadFromFile("assets/player/player2Big.png")) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}

	// Animations
	// Bombs
	if (!this->bomb1.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(64, 288, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->bomb2.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(96, 288, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->bomb3.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(112, 288, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->bomb4.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(128, 288, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	this->bombAnim = { bomb1, bomb2, bomb3, bomb4 };

	// Explosions
	if (!this->explosion1.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(224, 256, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->explosion2.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(224, 272, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	if (!this->explosion3.loadFromFile("assets/spritesheets/bomberman_sheet.png", sf::IntRect(224, 288, 16, 16))) {
		std::cout << "Could not load walkable tile texture" << std::endl;
	}
	this->explosionAnim = { explosion1, explosion2, explosion3 };
}
