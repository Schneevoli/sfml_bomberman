#pragma once

#include <string>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "../headers/main.h"
#include "../headers/input.h"
#include "../headers/gameObject.h"
#include "../headers/player.h"
#include "../headers/level.h"
#include "../headers/resource.h"
#include "../headers/gameState.h"
#include "../headers/explosion.h"


int main()
{
	// Here we're getting the current desktop size for small aspect ratio calculations later on
	unsigned int desktopWidth = sf::VideoMode::getDesktopMode().width;
	unsigned int desktopHeight = sf::VideoMode::getDesktopMode().height;
	float windowScale = desktopHeight / 1080;
	float windowResolution = 900;

	std::cout << "Resolution: " << windowResolution * windowScale << "x" << windowResolution * windowScale << std::endl;

	// Instantiate RenderWindow
	const unsigned int FPS = 60; //The desired FPS
	sf::RenderWindow window(sf::VideoMode(windowResolution * windowScale, windowResolution * windowScale), "SFML Bomberman");
	// This handles time management for us, although it is 
	// NOT a real fixed-step update which means in edge cases 
	// this could lead to some errors although it should be fine for 
	// our very simple games
	// Refer here for an explanation: https://gafferongames.com/post/fix_your_timestep/
	window.setFramerateLimit(FPS);

	// Initialize resource holder
	// This is necessary as otherwise textures would be 
	// killed on runtime and we would get the white-square problem
	// Refer to this for more information: 
	// https://www.sfml-dev.org/tutorials/2.3/graphics-sprite.php#the-white-square-problem
	Resource* resource = Resource::getInstance();
	resource->initializeTextures();

	// Define start positions for players
	sf::Vector2f startPosition1 = sf::Vector2f(400.f, 800.f);
	sf::Vector2f startPosition2 = sf::Vector2f(1200.f, 800.f);
	// Instantiate players
	Player player1 = Player(sf::Color::Green, 1, startPosition1, "Player 1");
	Player player2 = Player(sf::Color::Red, 2, startPosition2, "Player 2");

	Input input;
	// This vector represents the level's tilemap
	// 0 is a walkable tile, 1 is noneWalkable tile and 2 is a destructible tile
	// This vector's size should be 15*15 to fully fill out the playing field
	vector<int> levelMap = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 2, 2, 0, 1, 1, 0, 2, 2, 0, 0, 0, 1,
							 1, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 1,
							 1, 0, 0, 2, 2, 0, 1, 1, 0, 2, 2, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
							 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
	Level level(levelMap);

	// Declare a gameState variable for switching between different game states
	GameState gameState;

	/*
	* Main Game loop
	*/
	while (window.isOpen())
	{
		// The following is mainly used for listening to player input
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type) {
			case sf::Event::KeyReleased:
			{
				break;
			}
			case sf::Event::KeyPressed:
			{
				break;
			}
			case sf::Event::MouseButtonPressed:
			{
				break;
			}
			case sf::Event::Closed:
			{
				window.close();
				break;
			}
			default:
			{
				break;
			}
			}
		}

		window.clear(sf::Color::Black);

		// This is the start of the game, only the level 
		// and the logo should be displayed
		if (GameState::State == GameState::StateTypes::START) {
			gameState.setHasReinitialized();
			input.updateMenu(gameState);
			window.draw(gameState.inputActionSolicitation);
			window.draw(gameState.logo);
		}

		// This is the actual state while playing
		if (GameState::State == GameState::StateTypes::GAME) {
			input.update(player1, player1.playerId);
			input.update(player2, player2.playerId);
			player1.update();
			player2.update();

			// Update all current bombs
			for (int i = 0; i < BombComponent::currentBombs.size(); i++) {
				BombComponent::currentBombs[i].update(i);
			}
			// Update all current explosions
			for (int i = 0; i < Explosion::currentExplosions.size(); i++) {
				Explosion::currentExplosions[i].update(i);
			}

			window.draw(level); 
			window.draw(player1);
			window.draw(player2);

			// Draw all current bombs
			for (int i = 0; i < BombComponent::currentBombs.size(); i++) {
				window.draw(BombComponent::currentBombs[i]);
			}
			// Draw all current explosions
			for (int i = 0; i < Explosion::currentExplosions.size(); i++) {
				window.draw(Explosion::currentExplosions[i]);
			}

			// Draw player health texts
			window.draw(gameState.player1Health);
			window.draw(gameState.player2Health);
		}

		// This is the state when one player has won
		if (GameState::State == GameState::StateTypes::END) {
			input.updateMenu(gameState);
			window.draw(gameState.winnerDisplay);
			window.draw(gameState.inputActionSolicitation);
			gameState.reInitialize(player1, player2, level);
		}

		// GameState updates in all gameStates
		gameState.update(player1, player2);

		window.display();
	}

	return 0;
}